import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class User {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'first_name' })
  name: string;

  @Column({ name: 'last_name' })
  lastname: string;

  @Column({ unique: true })
  email: string;

  @Column()
  gender: string;

  @Column({ name: 'ip_address' })
  ipAddress: string;

  @Column()
  avatar: string;

}