import { Connection } from "typeorm";
import { User } from "../entity/User";
import { IUser } from "../model/IUser";

export class UserAPI {

  private connection: Connection;
  private DEFAULT_OFFSET = 0;
  private DEFAULT_LIMIT = 10;

  constructor(connection: Connection) {
    this.connection = connection
  }

  /**
   * Recupera registros de la BD con el desplazamiento especificado
   * @param offset cantidad de elementos a saltarse. Valor por defecto 0
   * @param limit cantidad de elementos a recuperar. Valor por defecto 10
   */
  async getUsers(desplazamientos?: { offset?: number, limit?: number }): Promise<Array<IUser>> {
    let offset = desplazamientos && desplazamientos.offset
    let limit = desplazamientos && desplazamientos.limit
    offset = isNaN(offset) ? this.DEFAULT_OFFSET : offset
    limit = isNaN(limit) ? this.DEFAULT_LIMIT : limit
    return await this.connection.manager.find(User, {
      skip: offset,
      take: limit
    });
  }

  async getUser(id: number): Promise<IUser> {
    return await this.connection.manager.findOne(User, {
      where: { id },
    });
  }

  async saveUser(user: IUser): Promise<IUser> {
    return await this.connection.manager.save(User, user);
  }

  async updateUser(user: IUser): Promise<IUser> {
    let currentUser = await this.getUser(user.id)
    // 1) lógica para actualizar nodos
    // 2) fusión de nodos
    currentUser = { ...currentUser, ...user }
    // 3) persistencia
    return await this.connection.manager.save(User, currentUser)
  }

  // async deleteUser(id: number): Promise<IUser> {
  //   let currentUser = await this.getUser(id)
  //   return await this.connection.manager.remove(User, currentUser)
  // }

}