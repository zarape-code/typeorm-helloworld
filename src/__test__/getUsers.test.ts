import { Connection } from "typeorm";
import { UserAPI } from "../api/UserAPI";
import { getDBConnection } from "../db";

let connection: Connection;
let userAPI: UserAPI;
beforeAll(async () => {
  // se inicializa la conexión a la BD
  connection = await getDBConnection()
  userAPI = new UserAPI(connection)
});

test('obtiene 10 usuarios de la BD (default values)', async () => {
  expect((await userAPI.getUsers()).length).toBe(10);
});
test('obtiene 5 usuarios de la BD pasando solo "limit"', async () => {
  expect((await userAPI.getUsers({ limit: 5 })).length).toBe(5);
});
test('obtiene 10 usuarios de la BD pasando solo "offset" (limit es 10 por defecto, solo salta los especificados en offset)', async () => {
  expect((await userAPI.getUsers({ offset: 7 })).length).toBe(10);
});
test('obtiene 3 usuarios de la BD', async () => {
  expect((await userAPI.getUsers({ offset: 0, limit: 3 })).length).toBe(3);
});
test('obtiene 3 usuarios de la BD, sin considerar los 3 primeros', async () => {
  const res = await userAPI.getUsers({ offset: 3, limit: 3 })
  expect(res.length).toBe(3);
  expect(res[0].id).toBe(4);  // id inferior 4
  expect(res[res.length - 1].id).toBe(6);  // id superior 6
});
test('obtiene 5 usuarios de la BD, sin considerar los 3 primeros', async () => {
  const res = await userAPI.getUsers({ offset: 3, limit: 5 })
  expect(res.length).toBe(5);
  expect(res[0].id).toBe(4);  // id inferior 4
  expect(res[res.length - 1].id).toBe(8);  // id superior 8
});