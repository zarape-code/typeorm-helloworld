# TypeORM Hello World

Proyecto que muestra la configuración y uso de [TypeORM](https://typeorm.io/) a través de pruebas unitarias por lo que al mismo tiempo se muestra cómo configurar [Jest](https://jestjs.io/) con [TypeScript](https://www.typescriptlang.org/)

## Ejecutar el proyecto
```sh
$ npm t
```


## Dependencias
En este proyecto se está usando una BD SQLite por lo que la dependencia `sqlite3` tiene sentido pero en su momento debe ajustarse por la necesaria para la BD que se esté usando
```sh
$ npm i reflect-metadata sqlite3 typeorm
```

Dependencias de desarrollo
```sh
$ npm i -D @types/jest jest ts-jest ts-node typescript
```

## Tecnología empleada
* [TypeScript](https://www.typescriptlang.org/) - Tipos para JS
* [SQLite](https://www.sqlite.org/) - Base de datos
* [TypeORM](https://typeorm.io/) - ORM
* [Jest](https://jestjs.io/) - Motor de pruebas unitarias

## Extensiones de VSCode usadas
* [vscode-sqlite](https://github.com/AlexCovizzi/vscode-sqlite) - SQLite explorer

## Mock data
Los 1,000 registros almacenados en `database.sqlite` fueron obtenidos de [Mockaroo](https://www.mockaroo.com/)
___
### Autor
Creado por [Ethien Salinas](https://www.linkedin.com/in/ethiensalinas/) para zarape.io 🇲🇽